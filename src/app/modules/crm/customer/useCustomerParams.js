export const useCustomerParamsInit = {
        /** Paginate Query */
        page: 1,
        per_page: 10,
    
        /** Sorting Query */
        order: "id",
        sort: "asc",
    
        /** Search Query */
        search: "",
        columns: "id,full_name,email,phone,company_name,position",
}