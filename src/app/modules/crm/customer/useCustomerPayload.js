export const useCustomerPayload = {
    customerCreate: {
        profile_picture : '',
        full_name: "",
        email: "",
        phone: '',
        company_name: '',
        position: ""
    },
    customerCreateAddress : {
        customer_id: '',
        address: ''
    },
    customerUpdate: {
        profile_picture : '',
        full_name: "",
        email: "",
        phone: '',
        company_name: '',
        position: "",
        status: ""
    }
}