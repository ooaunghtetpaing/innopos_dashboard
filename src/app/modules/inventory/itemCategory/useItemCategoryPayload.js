export const useItemCategoryPayload = {
    itemCategoryCreate: {
        name: "",
        remark: ""
    },
    itemCategoryUpdate: {
        name: "",
        remark: "",
        status: ""
    },
}