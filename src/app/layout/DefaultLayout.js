import { AppShell, Container, MantineProvider } from '@mantine/core';
import { useLocalStorage } from '@mantine/hooks';
import { Outlet } from 'react-router-dom';
import { DefaultHeader } from './components/DefaultHeader';
import { DefaultNavigation } from './components/DefaultNavigation';
import { AppNotification } from "../components/AppNotification";

export const DefaultLayout = () => {

    const [schema] = useLocalStorage({defaultValue: null, key: "color-schema"});

    return(
        <MantineProvider 
            withGlobalStyles 
            withNormalizeCSS 
            theme={{
                colorScheme: schema ? schema : "light"
            }}
        >
            <Container 
                fluid sx={{
                    backgroundColor: "#FAFBFC",
                    minHeight: "100vh"
                }} 
                p={0}
            >
                <AppShell
                    padding="md"
                    navbar={ <DefaultNavigation />}
                    header={<DefaultHeader />}
                >
                    <AppNotification />
                    <Outlet />
                </AppShell>
            </Container>
            
        </MantineProvider>
    )
}